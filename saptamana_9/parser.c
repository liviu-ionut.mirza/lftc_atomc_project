#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include "lexer.h"
#include "parser.h"

extern const char *tokenNames[];

Token *iTk;         // iteratorul în lista de tokenuri
Token *consumedTk;  // ultimul token consumat

void tkerr(const char *fmt,...){
	fprintf(stderr,"error in line %d: ",iTk->line);
    va_list va;
    va_start(va,fmt);
    vfprintf(stderr,fmt,va);
    va_end(va);
    fprintf(stderr,"\n");
    exit(EXIT_FAILURE);
}


bool consume(int code){
    printf("Consume: %s", tokenNames[code]); 

    if(iTk->code == code){
        consumedTk = iTk;
        iTk = iTk->next;
        printf(" => Consumed: %s\n", tokenNames[code]); 
        return true;
    } else {
        printf(" => found: %s\n", tokenNames[iTk->code]); 
        return false;
    }
}

bool typeBase() {
    Token *start = iTk;
    if (consume(TYPE_INT) || consume(TYPE_DOUBLE) || consume(TYPE_CHAR)) {
        return true;
    } else if (consume(STRUCT)) {
        if (consume(ID)) {
            return true;
        } else {
            tkerr("lipseste numele structurii");
        }
    }
    iTk = start; 
    return false;
}

bool arrayDecl() {
    Token *start = iTk;
    if (consume(LBRACKET)) {
        if(consume(INT)){} 
        if (consume(RBRACKET)) {
            return true;
        } else {
            tkerr("lipseste ] la declararea array-ului");
        }
    }
    iTk = start;
    return false;
}

bool varDef() {
    Token *start = iTk;
    if (typeBase()) {
        if (consume(ID)) {
            if(arrayDecl()){} 
            if (consume(SEMICOLON)) {
                return true;
            } else {
                tkerr("lipseste ; dupa declararea variabilei");
            }
        } else {
            tkerr("lipseste identificatorul după tipul de baza la declararea variabilei");
        }
    }
    iTk = start;
    return false;
}

bool structDef(){
	Token* start = iTk;
	if(consume(STRUCT)){
		if(consume(ID)){
			if(consume(LACC)){
				while(varDef()){
				}
				if(consume(RACC)){
					if(consume(SEMICOLON))
					{
						return true;
					} else {
                        tkerr("lipseste ; dupa definitia structurii");
                    }
                } else {
                    tkerr("lipseste '}' la sfarșitul definitiei structurii");
                }
            } else {
                tkerr("lipseste '{' dupa identificatorul structurii");
            }
        } else {
            tkerr("lipseste identificatorul după cuvantul cheie STRUCT");
        }
    }
    iTk = start;
    return false;
}

bool fnDef() {
    Token *start = iTk;
    if (typeBase() || consume(VOID)) {
        if (consume(ID)) {
            if (consume(LPAR)) {
                // Verificăm dacă există parametri
                if (!consume(RPAR)) { // Încercăm să închidem paranteza mai întâi pentru a gestiona cazul fără parametri
                    do {
                        if (!fnParam()) {
                            tkerr("Parametru asteptat în definitia functiei");
                        }
                        // Dacă am citit un parametru, încercăm să citim ',' sau ')'
                    } while (consume(COMMA));
                    
                    if (!consume(RPAR)) {
                        tkerr("Lipseste ')' dupa lista de parametri");
                    }
                }
                if (stmCompound()) {
                    return true;
                } else {
                    tkerr("Lipseste corpul functiei");
                }
            } else {
                tkerr("Lipseste '(' dupa identificatorul functiei");
            }
        } else {
            tkerr("Lipseste identificatorul dupa tipul de intoarcere al functiei");
        }
    }
    iTk = start;
    return false;
}


bool fnParam() {
    Token *start = iTk;
    if (typeBase()) {
        if (consume(ID)) {
            arrayDecl(); // Consumul este opțional, deci nu e nevoie să verificăm dacă eșuează
            return true;
        } else {
            tkerr("Identificatorul parametrului asteptat după tipul de baza");
        }
    }
    iTk = start;
    return false;
}

bool stm(){
	Token* start= iTk;
	if(stmCompound()){
		return true;
	}
	if(consume(IF)){
		if(consume(LPAR)){
			if(expr()){
				if(consume(RPAR)){
					if(stm()){
						if(consume(ELSE)){
							if(stm()){}else tkerr("else statement mssing");
						}
						return true;
					}else tkerr("if statement mssing");
				}else tkerr("Missing ) for if");
			}else tkerr("Missing if expression");
		}else tkerr("Missing ( after if");
		iTk=start;
	}
	else if(consume(WHILE)){
		if(consume(LPAR)){
			if(expr()){
				if(consume(RPAR)){
					if(stm()){
						return true;
					}else tkerr("Missing while body");
				}else tkerr("Missing ) for while");
			}else tkerr("Missing while condition");
		}else tkerr("Missing ( after while keyword");
		iTk=start;
	}
	else if(consume(RETURN)){
		if(expr()){}
		if(consume(SEMICOLON)){
			return true;
		}else tkerr("missing ; at return statement");
		iTk=start;
	}
	else if(expr())
	if(consume(SEMICOLON)){
		return true;
	}
	iTk = start;
	return false;
}

/*

bool stm() {
    Token *start = iTk;
    if (stmCompound()) {
        return true;
    } else if (consume(IF)) {
        if (consume(LPAR)) {
            if (expr()) {
                if (consume(RPAR)) {
                    if (stm()) {
                        if (consume(ELSE)) {
                            if (!stm()) {
                                tkerr("Blocul ELSE asteptat dupa blocul IF");
                            }
                        }
                        return true;
                    } else {
                        tkerr("Blocul IF așteptat dupa paranteza închisa");
                    }
                } else {
                    tkerr("Paranteza dreapta asteptata dupa conditia IF");
                }
            } else {
                tkerr("Conditie asteptata in IF");
            }
        } else {
            tkerr("Paranteza stanga asteptata dupa IF");
        }
    } else if (consume(WHILE)) {
        if (consume(LPAR)) {
            if (expr()) {
                if (consume(RPAR)) {
                    if (stm()) {
                        return true;
                    } else {
                        tkerr("Lipseste corpul buclei WHILE");
                    }
                } else {
                    tkerr("Lipseste ')' dupa conditia WHILE");
                }
            } else {
                tkerr("Condiție așteptată în WHILE");
            }
        } else {
            tkerr("Lipsește '(' după cuvântul cheie WHILE");
        }
    } else if (consume(RETURN)) {
        expr(); // Expresia este opțională
        if (!consume(SEMICOLON)) {
            tkerr("SEMICOLON așteptat după RETURN");
        }
        return true;
    } else if (expr()) {
        if (!consume(SEMICOLON)) {
            tkerr("SEMICOLON așteptat după expresie");
        }
        return true;
    }
    iTk = start;
    return false;
}

*/

bool stmCompound(){
	Token* start = iTk;
	if(consume(LACC)){
		for(;;){
			if(varDef()){}
			else if(stm()){}
			else break;
		}
		if(consume(RACC)){
			return true;
		}else tkerr("asteptat la inchiderea unui bloc de instructiuni");
	}
	iTk = start;
	return false;
}

bool expr(){
    Token *start = iTk;
	if(exprAssign()){
		return true;
	}
	iTk = start;
	return false;
}

bool exprAssign(){
	Token *start = iTk;
	if(exprUnary()){
		if(consume(ASSIGN)){
			if(exprAssign()){
				return true;
			}else tkerr("Expresie asteptata dupa operatorul de atribuire.");
		}
		iTk=start;
	}
	if(exprOr()){
		return true;
	}
	iTk = start;
	return false;
}




bool exprOr() {
    Token* start = iTk;
    if(exprAnd()){
        if(exprOrPrim()){
            return true;
        }
    }
    iTk = start;
    return false;
}

bool exprOrPrim() {
    if (consume(OR)) {
        if (exprAnd()) {
            if (exprOrPrim()) {
                return true;
            }
        } else {
            tkerr("Expresie așteptată după operatorul OR.");
        }
    }
    return true; // epsilon
}


bool exprAnd() {
    Token* start = iTk;
    if(exprEq()){
        if(exprAndPrim()){
            return true;
        }
    }
    iTk = start;
    return false;
}

bool exprAndPrim() {
    if (consume(AND)) {
        if (exprEq()) {
            if (exprAndPrim()) {
                return true;
            }
        } else {
            tkerr("Expresie așteptată după operatorul AND.");
        }
    }
    return true; // epsilon
}


bool exprEq() {
    
    Token* start = iTk;
    if(exprRel()){
        if(exprEqPrim()){
            return true;
        }
    }
    iTk = start;
    return false;
}

bool exprEqPrim() {
   
    if (consume(EQUAL) || consume(NOTEQ)) {
        if (exprRel()) {
            if (exprEqPrim()) {
                return true;
            }
        } else {
            tkerr("Expresie așteptată după operatorul de egalitate sau inegalitate.");
        }
    }
    return true; // epsilon
}


bool exprRel() {
    Token* start = iTk;
    if(exprAdd()){
        if(exprRelPrim()){
            return true;
        }
    }
    iTk = start;
    return false;
}

bool exprRelPrim() {
    if (consume(LESS) || consume(LESSEQ) || consume(GREATER) || consume(GREATEREQ)) {
        if (exprAdd()) {
            if (exprRelPrim()) {
                return true;
            }
        } else {
            tkerr("Expresie așteptată după operatorul relațional.");
        }
    }
    return true; // epsilon
}


bool exprAdd() {
    Token* start = iTk;
    if(exprMul()){
        if(exprAddPrim()){
            return true;
        }
    }
    iTk = start;
    return false;
}

bool exprAddPrim() {
    if (consume(ADD) || consume(SUB)) {
        if (exprMul()) {
            if (exprAddPrim()) {
                return true;
            }
        } else {
            tkerr("Expresie așteptată după operatorul de adunare/scădere.");
        }
    }
    return true; // epsilon
}


bool exprMul() {
    Token* start = iTk;
    if(exprCast()){
        if(exprMulPrim()){
            return true;
        }
    }
    iTk = start;
    return false;
}

bool exprMulPrim() {
    if (consume(MUL) || consume(DIV)) {
        if (exprCast()) {
            if (exprMulPrim()) {
                return true;
            }
        } else {
            tkerr("Expresie așteptată după operatorul de înmulțire/împărțire.");
        }
    }
    return true; // epsilon, pentru a gestiona cazul în care nu există mai multe înmulțiri sau împărțiri consecutive
}


bool exprCast(){
    Token *start = iTk;
    if(consume(LPAR)){
        if(typeBase()){
            if(arrayDecl()){} // Opțional
            if(consume(RPAR)){
                if(exprCast()){
                    return true;
                } else {
                    tkerr("Exprimare așteptată după conversia de tip.");
                }
            } else {
                tkerr("Paranteză închisă așteptată după tipul de bază în conversia de tip.");
            }
        } else {
            tkerr("Tip de bază așteptat în conversia de tip.");
        }
    } 
    if(exprUnary()){	
		return true;
	}
    iTk = start;
    return false;
}


bool exprUnary(){
    Token* start = iTk;
    if(consume(SUB) || consume(NOT)){
        if(!exprUnary()){
            tkerr("Expresie asteptata dupa operatorul unar.");
        }
        return true;
    } 
    if(exprPostfix()){
		return true;
	}
    iTk = start;
    return false;
}



bool exprPrimary(){
    Token* start = iTk;
    if(consume(ID) || consume(INT) || consume(DOUBLE) || consume(CHAR) || consume(STRING)){
        if(consume(LPAR)){
            if(expr()){
                while(consume(COMMA)){
                    if(!expr()){
                        tkerr("Expresie asteptata dupa virgula in lista de argumente.");
                    }
                }
                if(!consume(RPAR)){
                    tkerr("Lipseste paranteza inchisa dupa lista de argumente.");
                }
            }
            return true;
        }
        return true; // pentru ID fără apel de funcție
    } else if(consume(LPAR)){
        if(expr()){
            if(!consume(RPAR)){
                tkerr("Lipseste paranteza dreapta dupa expresie.");
            }
            return true;
        }
    }
    iTk = start;
    return false;
}


bool exprPostfix() {
    Token* start = iTk;
    if(exprPrimary()){
        if(exprPostfixPrim()){
            return true;
        }
    }
    iTk = start;
    return false;
}

bool exprPostfixPrim(){
    if(consume(LBRACKET)){
        if(expr()){
            if(consume(RBRACKET)){
                if(exprPostfixPrim()){
                    return true;
                }
            } else tkerr("Lipseste ] dupa expresie în indexarea array-ului.");
        }
    } else if(consume(DOT)){
        if(consume(ID)){
            if(exprPostfixPrim()){
                return true;
            }
        } else tkerr("Lipseste identificatorul dupa operatorul '.'.");
    }
    return true;
}


bool unit(){
	for(;;){//sa putem verifica oricate reguli pentr "*", repetitie de la 1 la infinit
		if(structDef()){}
		else if(fnDef()){}
		else if(varDef()){}
		else break;
		}
	if(consume(END)){
		return true;
		}
	return false;
}

void parse(Token *tokens){
    iTk = tokens;
    if(!unit()){
        tkerr("syntax error");
    }
}
