#pragma once

enum {
    ID,
    TYPE_CHAR,
    COMMA,
    END,
    ASSIGN,
    LPAR,
    RPAR,
    LBRACKET,
    RBRACKET,
    LACC,
    RACC,
    ADD,
    SUB,
    MUL,
    DIV,
    DOT,
    AND,
    OR,
    NOT,
    NOTEQ,
    EQUAL,
    LESSEQ,
    LESS,
    GREATEREQ,
    GREATER,
    TYPE_DOUBLE,
    ELSE,
    IF,
    TYPE_INT,
    RETURN,
    STRUCT,
    VOID,
    WHILE,
    CHAR,
    STRING,
    DOUBLE,
    INT,
    SEMICOLON
};

typedef struct Token {
    int code;
    int line;
    union {
        char *text;
        int i;
        char c;
        double d;
    };
    struct Token *next;
} Token;

Token *tokenize(const char *pch);
void showTokens(const Token *tokens);
void freeTokens(Token *tokens);
