#pragma once

#include "lexer.h"
#include <stdbool.h>


void tkerr(const char *fmt,...);
bool consume(int code);
bool typeBase();
bool arrayDecl();
bool varDef();
bool structDef();
bool fnDef();
bool stmCompound();
bool fnParam();
bool stm();
bool expr();
bool exprAssign();
bool exprAssignPrim();
bool exprOr();
bool exprOrPrim();
bool exprAnd();
bool exprAndPrim();
bool exprEq();
bool exprEqPrim();
bool exprRel();
bool exprRelPrim();
bool exprAdd();
bool exprAddPrim();
bool exprMul();
bool exprMulPrim();
bool exprCast();
bool exprUnary();
bool exprPostfix();
bool exprPostfixPrim();
bool exprPrimary();
bool unit();
void parse(Token *tokens);
