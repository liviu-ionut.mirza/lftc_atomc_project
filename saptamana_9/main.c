
#include <stdio.h>
#include <stdlib.h>

#include "lexer.h"
#include "parser.h"
#include "utils.h"

int main() {
    //char *inputCode = loadFile("tests/testlex.c");
    char *inputCode = loadFile("tests/testparser.c");
    Token *tokenList = tokenize(inputCode);
    showTokens(tokenList);
    printf("\n\n");
    parse(tokenList);
    freeTokens(tokenList);
    free(inputCode);

    return 0;
}
