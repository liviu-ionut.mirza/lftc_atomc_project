#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#include "lexer.h"
#include "utils.h"

Token *tokens;	// single linked list of tokens
Token *lastTk;		// the last token in list

int line=1;		// the current line in the input file
int ok=0;

// adds a token to the end of the tokens list and returns it
// sets its code and line
Token *addTk(int code) {
    Token *tk = safeAlloc(sizeof(Token));
    tk->code = code;
    tk->line = line;
    tk->next = NULL;
    if (lastTk) {
        lastTk->next = tk;
    } else {
        tokens = tk;
    }
    lastTk = tk;
    return tk;
}

char *extract(const char *begin, const char *end) {
    int length = end - begin ;
    char *result = safeAlloc(length + 1);
    strncpy(result, begin, length);
    result[length] = '\0';
    return result;
}

const char *tokenNames[] = {
    "ID",
    "TYPE_CHAR",
    "COMMA",
    "END",
    "ASSIGN",
    "LPAR",
    "RPAR",
    "LBRACKET",
    "RBRACKET",
    "LACC",
    "RACC",
    "ADD",
    "SUB",
    "MUL",
    "DIV",
    "DOT",
    "AND",
    "OR",
    "NOT",
    "NOTEQ",
    "EQUAL",
    "LESSEQ",
    "LESS",
    "GREATEREQ",
    "GREATER",
    "TYPE_DOUBLE",
    "ELSE",
    "IF",
    "TYPE_INT",
    "RETURN",
    "STRUCT",
    "VOID",
    "WHILE",
    "CHAR",
    "STRING",
    "DOUBLE",
    "INT",
    "SEMICOLON"
};


void freeTokens(Token *tokens) {
    Token *current = tokens;
    Token *next;

    while (current != NULL) {
        next = current->next;
        if (current->code == ID || current->code == STRING) {
            free(current->text);
        }
        free(current);
        current = next;
    }
}



void showTokens(const Token *tokens) {
    const Token *current = tokens;

    while (current != NULL) {
        printf("%d\t", current->line);

        switch (current->code) {
            case TYPE_INT:
            case TYPE_CHAR:
            case TYPE_DOUBLE:
            case VOID:
                printf("%s\n", tokenNames[current->code]);
                break;
            case ID:
                printf("%s:%s\n", tokenNames[current->code], current->text);
                break;
            case INT:
                printf("%s:%d\n", tokenNames[current->code], current->i);
                break;
            case CHAR:
                printf("%s:%c\n", tokenNames[current->code], current->c);
                break;
            case DOUBLE:
                printf("%s:%lf\n", tokenNames[current->code], current->d);
                break;
            case STRING:
                printf("%s:%s\n", tokenNames[current->code], current->text);
                break;
            default:
                printf("%s\n", tokenNames[current->code]);
        }

        current = current->next;
    }
}

Token *tokenize(const char *pch) {
    const char *start;
    Token *tk;

    for (;;) {
    switch (*pch) {
        case ' ': case '\t': case '\r':
            pch++;
            break;
            case '\n':
                pch++;
                line++;
                break;
            case '\0': case EOF:
                addTk(END);
                return tokens;
            case ',':
                addTk(COMMA);
                pch++;
                break;
            case ';':
                addTk(SEMICOLON);
                pch++;
                break;
            case '(':
                addTk(LPAR);
                pch++;
                break;
            case ')':
                addTk(RPAR);
                pch++;
                break;
            case '[':
                addTk(LBRACKET);
                pch++;
                break;
            case ']':
                addTk(RBRACKET);
                pch++;
                break;
            case '{':
                addTk(LACC);
                pch++;
                break;
            case '}':
                addTk(RACC);
                pch++;
                break;
            case '+':
                addTk(ADD);
                pch++;
                break;
            case '-':
                addTk(SUB);
                pch++;
                break;
            case '*':
                addTk(MUL);
                pch++;
                break;
            case '.':
                addTk(DOT);
                pch++;
                break;
            case '&':
                if (pch[1] == '&') {
                    addTk(AND);
                    pch += 2;
                } else {
                    err("Invalid character: %c", *pch);
                }
                break;
            case '|':
                if (pch[1] == '|') {
                    addTk(OR);
                    pch += 2;
                } else {
                    err("Invalid character: %c", *pch);
                }
                break;
            case '!':
                if (pch[1] == '=') {
                    addTk(NOTEQ);  
                    pch += 2;     
                } else {
                    addTk(NOT);
                    pch++;
                }
                break;
            case '=':
                if (pch[1] == '=') {
                    addTk(EQUAL);
                    pch += 2;
                } else {
                    addTk(ASSIGN);
                    pch++;
                }
                break;
            case '<':
                if (pch[1] == '=') {
                    addTk(LESSEQ);
                    pch += 2;
                } else {
                    addTk(LESS);
                    pch++;
                }
                break;
            case '>':
                if (pch[1] == '=') {
                    addTk(GREATEREQ);
                    pch += 2;
                } else {
                    addTk(GREATER);
                    pch++;
                }
                break;
            case '/':
                if (pch[1] == '/') {
                    // Comentariu pe o linie
                    while (*pch && *pch != '\n') {
                        pch++;
                    }
                } else {
                    // Operator /
                    addTk(DIV);
                    pch++;
                }
                break;
            default:
                if (isalpha(*pch) || *pch == '_') {
                    for (start = pch++; isalnum(*pch) || *pch == '_'; pch++) {}
                    char *text = extract(start, pch);

                    if (strcmp(text, "char") == 0) {
                        addTk(TYPE_CHAR);
                    } else if (strcmp(text, "double") == 0) {
                        addTk(TYPE_DOUBLE);
                    } else if (strcmp(text, "else") == 0) {
                        addTk(ELSE);
                    } else if (strcmp(text, "if") == 0) {
                        addTk(IF);
                    } else if (strcmp(text, "int") == 0) {
                        addTk(TYPE_INT);
                    } else if (strcmp(text, "return") == 0) {
                        addTk(RETURN);
                    } else if (strcmp(text, "struct") == 0) {
                        addTk(STRUCT);
                    } else if (strcmp(text, "void") == 0) {
                        addTk(VOID);
                    } else if (strcmp(text, "while") == 0) {
                        addTk(WHILE);
                    } else {
                        tk = addTk(ID);
                        tk->text = text;
                        tk->line=line;
                        ok=1;
                    }
                    if(ok==0)
                    {
                    tk->line = line;
                    //ok=0;
                    }
                     
                } else if (isdigit(*pch)) {
                    for (start = pch++; isdigit(*pch); pch++) {}
                    if (*pch == '.') {
                        pch++;
                        while (isdigit(*pch)) {
                            pch++;
                        }
                    }
                    if (*pch == 'e' || *pch == 'E') {
                        pch++;
                        if (*pch == '+' || *pch == '-') {
                            pch++;
                        }
                        while (isdigit(*pch)) {
                            pch++;
                        }
                    }
                    char *text = extract(start, pch);
                    if (strchr(text, '.') || strchr(text, 'e') || strchr(text, 'E')) {
                        tk = addTk(DOUBLE);
                        tk->d = atof(text);
                    } else {
                        tk = addTk(INT);
                        tk->i = atoi(text);
                    }
                    free(text);
                } else if (*pch == '\'') {
                    start = ++pch;
                    if (*pch == '\\' && pch[1]) {
                        pch += 2;
                    } else {
                        pch++;
                    }
                    if (*pch == '\'') {
                        tk = addTk(CHAR);
                        tk->c = *start;
                        pch++;
                    } else {
                        err("Invalid character constant");
                    }
                } else if (*pch == '"') {
                    start = ++pch;
                    while (*pch && *pch != '\"') {
                        pch++; // Avansează până la ghilimele de închidere
                    }
                    if (*pch == '\"') {
                        tk = addTk(STRING);
                        tk->text = extract(start, pch);
                        pch++;
                    } else {
                        err("Unterminated string constant");
                    }
                } else {
                    err("Invalid character: %c", *pch);
                }
        }
    }
}